package gestionter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestGroupeSujet {
	Sujet s;
	Groupe g;
	@BeforeEach
	public void setUp() {
		 s= new Sujet("Math","Apprendre à compter");
		 g= new Groupe("Les mateux");
	}
	
	@Test
	public void testGroupeSujet() {
		g.setSujet(s);
		assertEquals(g.getSujet() ==s, true);
		assertEquals(s.getGroupe()==g, true);
	}
}
