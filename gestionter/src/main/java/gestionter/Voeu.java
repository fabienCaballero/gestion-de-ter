package gestionter;


public class Voeu {
	private int priorite;
	private Groupe Groupe;
	private Sujet Sujet;

	
	public Voeu(int priorite, Groupe groupe, Sujet sujet) throws Exception{
		if(priorite>=1 && priorite<=5)
			this.priorite = priorite;
		else 
			throw new Exception("priorité non valide");
		Groupe = groupe;
		Sujet = sujet;
	}

	public Groupe getGroupe() {
		return Groupe;
	}

	public void setGroupe(Groupe groupe) {
		Groupe = groupe;
	}

	public Sujet getSujet() {
		return Sujet;
	}

	public void setSujet(Sujet sujet) {
		Sujet = sujet;
	}

	public void setPriorite(int priorite) {
		this.priorite = priorite;
	}

	public int getPriorite() {
		return priorite;
	}
	

}
