package gestionter;


public class Sujet {
	private String nomSujet;
	private String description;
	private Groupe Groupe;
	
	
	
	public Sujet(String nomSujet, String description) {
		super();
		this.nomSujet = nomSujet;
		this.description = description;
		Groupe = null;
	}
	public Groupe getGroupe() {
		return Groupe;
	}
	public void setGroupe(Groupe groupe) {
		Groupe = groupe;
		Groupe.setSujet(this);
	}

}
