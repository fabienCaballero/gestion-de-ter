package gestionter;


import java.util.ArrayList;

public class Groupe {
	private String nomGroupe;
	private Sujet Sujet;
	private ArrayList<Voeu> Voeux;
	private boolean premPhaseTerminee; 
	
	

	public Groupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
		this.Sujet = null;
	}
	public void addVoeu(Sujet Sujet,int priorite) {
		try {
			Voeux.add(new Voeu(priorite, this, Sujet));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getNomGroupe() {
		return nomGroupe;
	}

	public void setNomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}

	public ArrayList<Voeu> getVoeu() {
		return Voeux;
	}

	public void setVoeu(ArrayList<Voeu> voeu) {
		Voeux = voeu;
	}

	public boolean isPremPhaseTerminee() {
		return premPhaseTerminee;
	}

	public void setPremPhaseTerminee(boolean premPhaseTerminee) {
		this.premPhaseTerminee = premPhaseTerminee;
	}

	

	public Sujet getSujet() {
		return Sujet;
	}
	public void setSujet(Sujet Sujet) {
		this.Sujet = Sujet;
		Sujet.setGroupe(this);
	}

	public boolean echecAffectation() {
		
		return true;
		
	}
	
	

}
